#pragma once

#include <glm/glm.hpp>

#include <Camera.hpp>
#include <Mesh.hpp>
#include <Material.hpp>
#include <Transform.hpp>
#include <Light.hpp>

namespace Engine
{
    class RenderObject
    {
    private:
        Transform _transform;
        bool _is_selectable;
        int _index;

    protected:
        RenderObject();
        Mesh* _mesh;
        Material* _material;
		
		RenderObject *parent = NULL;

    public:
        RenderObject(Mesh* mesh, Material* material);
        void Render(Camera* cam);
        void Render(Light* light);
        void SetMesh(Mesh* mesh) { _mesh = mesh; }
        void SetMaterial(Material* material) { _material = material; }
        
        Transform* GetTransform() { return &_transform; }
        void SetTransform(Transform t) { _transform = t; }
        void EnableSelection(int index) {_is_selectable = true; _index = index;};
        void DisableSelection() {_is_selectable = false;};
    };
}