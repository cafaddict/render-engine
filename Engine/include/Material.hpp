#pragma once

#include <glm/glm.hpp>

#include <Program.hpp>
#include <Light.hpp>

namespace Engine
{
    class Material
    {
    protected:
        Program* _program;
        Program* _shadow_program;
    public:
        Material();
        ~Material();
        Program *GetProgram() { return _program; }
        Program *GetShadowProgram() { return _shadow_program;};
        void UpdateLight(std::vector<Light> &lights);
    };
}