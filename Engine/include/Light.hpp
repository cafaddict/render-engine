#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <Transform.hpp>

#define MAX_LIGHTS 3
// Feel free to increase maximum number of lights!


namespace Engine
{
	enum LightType : int
	{
		PointLight = 1,
		DirectionalLight = 2,
		Spotlight = 3,
	};

	struct LightProjectionData
    {
        float left = -10.0f;
		float right = 10.0f;
		float bottom = -10.0f;
		float top = 10.0f;
        float zNear = 0.01f;
        float zFar = 1000.0f;
    };

	class Light
	{
	public:
		LightType type;
		int enabled = 1;		

		//Point light & spotlight
		Transform transform;
		LightProjectionData projection;

		//Common
		glm::vec3 diffuse_illuminance;
		glm::vec3 ambient_illuminance;
		glm::vec3 specular_illuminance;

		//Point light
		float constant;
		float linear;
		float quadratic;

		//Spot light
		float cutOff;
		float outerCutOff;

		//Directional & Spot light
		glm::vec3 direction;

		LightProjectionData GetProjection() {return projection;};
        glm::mat4 GetProjectionMatrix() { return glm::ortho(projection.left, projection.right, projection.bottom, projection.top, projection.zNear, projection.zFar); }
	};
};

