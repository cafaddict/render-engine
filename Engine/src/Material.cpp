#include <Material.hpp>
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define BUF_SIZE 50
namespace Engine
{
    Material::Material()
    {
        _program = new Program();
        _shadow_program= new Program();
    }

    Material::~Material()
    {
        delete _program;
    }

    void Material::UpdateLight(std::vector<Light> &lights)
    {
        GLuint pid = _program->GetProgramId();
        glUseProgram(pid);
        
        
        int numLights = MIN(lights.size(), MAX_LIGHTS);
        GLuint location = glGetUniformLocation(pid, "numLights");
        glUniform1i(location, numLights);

        char buf[BUF_SIZE];
        for (int i = 0; i < numLights; i++)
        {
            snprintf(buf, BUF_SIZE, "lights[%d].diffuse_illuminance", i);
            location = glGetUniformLocation(pid, buf);
            glUniform3fv(location, 1, (float*)&(lights[i].diffuse_illuminance));

            snprintf(buf, BUF_SIZE, "lights[%d].ambient_illuminance", i);
            location = glGetUniformLocation(pid, buf);
            glUniform3fv(location, 1, (float*)&(lights[i].ambient_illuminance));

            snprintf(buf, BUF_SIZE, "lights[%d].specular_illuminance", i);
            location = glGetUniformLocation(pid, buf);
            glUniform3fv(location, 1, (float*)&(lights[i].specular_illuminance));

            glm::mat4 world_transform = lights[i].transform.GetWorldTransform();
            glm::vec4 local_pos = glm::vec4(0.0, 0.0, 0.0, 1.0);
            glm::vec4 world_pos = world_transform * local_pos;
            snprintf(buf, BUF_SIZE, "lights[%d].pos", i);
            location = glGetUniformLocation(pid, buf);
            glUniform3fv(location, 1, (float*)&(world_pos));

            
            snprintf(buf, BUF_SIZE, "lights[%d].type", i);
            location = glGetUniformLocation(pid, buf);
            glUniform1i(location, lights[i].type);



            switch(lights[i].type)
            {
                case PointLight:
                    snprintf(buf, BUF_SIZE, "lights[%d].constant", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].constant);

                    snprintf(buf, BUF_SIZE, "lights[%d].linear", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].linear);

                    snprintf(buf, BUF_SIZE, "lights[%d].quadratic", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].quadratic);
                    break;

                case DirectionalLight:
                    snprintf(buf, BUF_SIZE, "lights[%d].direction", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform3fv(location, 1, (float*)&(lights[i].direction));
                    break;

                case Spotlight:
                    snprintf(buf, BUF_SIZE, "lights[%d].constant", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].constant);

                    snprintf(buf, BUF_SIZE, "lights[%d].linear", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].linear);

                    snprintf(buf, BUF_SIZE, "lights[%d].quadratic", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].quadratic);
                    
                    snprintf(buf, BUF_SIZE, "lights[%d].direction", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform3fv(location, 1, (float*)&(lights[i].direction));

                    snprintf(buf, BUF_SIZE, "lights[%d].cutOff", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].cutOff);

                    snprintf(buf, BUF_SIZE, "lights[%d].outerCutOff", i);
                    location = glGetUniformLocation(pid, buf);
                    glUniform1f(location, lights[i].outerCutOff);

                    break;


            }


        }
    }    
}