#include <DiffuseMaterial.hpp>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define BUF_SIZE 50

void DiffuseMaterial::CreateMaterial(int unit)
{
    _program->AttachShader("Resources/Materials/VertexShader.glsl", GL_VERTEX_SHADER);
    _program->AttachShader("Resources/Materials/DiffuseFragmentShader.glsl", GL_FRAGMENT_SHADER);
    _program->LinkShader();

    glUseProgram(_program->GetProgramId());
    GLuint location = glGetUniformLocation(_program->GetProgramId(), "diffuse_reflectance");
    glUniform3fv(location, 1, (float*)&color);
	location = glGetUniformLocation(_program->GetProgramId(), "ourTexture");
	glUniform1i(location, unit);

    _shadow_program->AttachShader("Resources/Materials/ShadowVertexShader.glsl", GL_VERTEX_SHADER);
    _shadow_program->AttachShader("Resources/Materials/ShadowFragmentShader.glsl", GL_VERTEX_SHADER);
    _shadow_program->LinkShader();
}

void DiffuseMaterial::UpdateDiffuseReflectance(glm::vec3 color)
{
    glUseProgram(_program->GetProgramId());
    GLuint location = glGetUniformLocation(_program->GetProgramId(), "diffuse_reflectance");
    glUniform3fv(location, 1, (float*)&color);
}

