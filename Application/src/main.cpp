// STL
#include <iostream>

// include opengl extension and application library
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

// include Engine
#include <Camera.hpp>
#include <Mesh.hpp>
#include <Material.hpp>
#include <RenderObject.hpp>
#include <ModelLoader.hpp>
#include <Texture.hpp>

#include <DiffuseMaterial.hpp>
#include <PickingMaterial.hpp>
#include <Geometry.hpp>
#include <ModelLoader.hpp>

#include <picking.hpp>
#include <PickableObject.hpp>

#include <CubeMaterial.hpp>
#include <Animation.hpp>

#include <shadow.hpp>

GLFWwindow* g_window;
float g_window_width = 1024.f;
float g_window_height = 768.f;
int g_framebuffer_width = 1024;
int g_framebuffer_height = 768;

Engine::Camera* main_camera;

Engine::Mesh* bunny_mesh;
Engine::Mesh* cube_mesh;

DiffuseMaterial* material;
CubeMaterial* cubemap_material;

//For Object Selection
PickingMaterial* picking_material;

// Engine::RenderObject* bunny_object; 
// Engine::RenderObject* cube_object;
PickableObject* bunny_object; 
PickableObject* cube_object;
Engine::RenderObject* cubemap_object;

std::vector<Engine::Light> lights;

Animation* bunny_animation;
Animation* camera_animation;
Animation* light_animation;

Engine::Transform* camT;

static void MouseButtonCallback(GLFWwindow* a_window, int a_button, int a_action, int a_mods)
{
	if (a_button == GLFW_MOUSE_BUTTON_LEFT && a_action == GLFW_PRESS)
	{
		double xpos, ypos;
		glfwGetCursorPos(a_window, &xpos, &ypos);
		xpos = xpos / ((double)g_window_width) * ((double)g_framebuffer_width);
		ypos = ypos / ((double)g_window_height) * ((double)g_framebuffer_height);
		int target = pick((int)xpos, (int)ypos, g_framebuffer_width, g_framebuffer_height);
		std::cout << "Picked object index: " << target << std:: endl;
	}
}

static void CursorPosCallback(GLFWwindow* a_window, double a_xpos, double a_ypos)
{

}

static void KeyboardCallback(GLFWwindow* a_window, int a_key, int a_scancode, int a_action, int a_mods)
{
	if (a_action == GLFW_PRESS)
	{
		switch (a_key)
		{
		case GLFW_KEY_H:
			std::cout << "Simple 3D model renderer" << std::endl;
			std::cout << "keymaps:" << std::endl;
			std::cout << "h\t\t Help command" << std::endl;
			std::cout << "r\t\t rotate the camera -30 degrees with y axis" << std::endl;
			std::cout << "l\t\t rotate the camera +30 degrees with y axis" << std::endl;
			break;
		case GLFW_KEY_L:
			camT = main_camera->GetTransform();
			camT->SetOrientation(glm::rotate(camT->GetOrientation(), glm::radians(30.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
			break;
		case GLFW_KEY_R:
			camT = main_camera->GetTransform();
			camT->SetOrientation(glm::rotate(camT->GetOrientation(), glm::radians(-30.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
			break;
		default:
			break;
		}
	}
}

static void FramebufferResizeCallback(GLFWwindow* a_window, int width, int height)
{
	glViewport(0, 0, width, height);
	reallocatePickingTexture(width, height);

	int window_width, window_height;
	glfwGetWindowSize(g_window, &window_width, &window_height);
	g_window_width = (float)window_width;
	g_window_height = (float)window_height;
	g_framebuffer_width = width;
	g_framebuffer_height = height;
	
	Engine::ProjectionData proj = main_camera->GetProjection();
	proj.aspectRatio = (float)width / (float)height;
	main_camera->SetProjection(proj.aspectRatio, proj.fov, proj.zNear, proj.zFar);
}

static void InitScene()
{
	main_camera = new Engine::Camera();
	main_camera->GetTransform()->SetPosition(glm::vec3(0.0f, 1.0f, 4.0f));

	Engine::ModelLoader *loader = new Engine::ModelLoader("Resources/Models/bunny.obj");

	bunny_mesh = new Engine::Mesh();
	cube_mesh = new Engine::Mesh();

	Geometry geometry = Geometry();
	geometry.GenerateCube(cube_mesh);
	
	loader->GenerateMesh(bunny_mesh, 0);
		
	//Load the texture from textureloader
	Engine::TextureLoader* textureloader1 = new Engine::TextureLoader(0, "Resources/Models/bunny.png");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);



	material = new DiffuseMaterial();
	material->CreateMaterial(0);

	//For Object Selection
	picking_material = new PickingMaterial();
	picking_material->CreateMaterial();

	// bunny_object = new Engine::RenderObject(bunny_mesh, material);
	bunny_object = new PickableObject(bunny_mesh, material);

	bunny_object->SetPickingMat(picking_material);
	bunny_object->SetIndex(1);

	Engine::Transform* bunny_T = bunny_object->GetTransform();
	bunny_T->SetPosition(glm::vec3(-1.5f, 0.8f, 0.0f));
	bunny_object->SetTransform(*bunny_T);



	std::string path_prefix = "Resources/Textures/skybox/";
	Engine::TextureLoader* textureloader2 = new Engine::TextureLoader(1, path_prefix + "right.jpg", path_prefix + "left.jpg", 
		path_prefix + "top.jpg", path_prefix + "bottom.jpg", path_prefix + "front.jpg", path_prefix + "back.jpg");
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	cubemap_material = new CubeMaterial();
	cubemap_material->CreateMaterial(1);
	cubemap_object = new Engine::RenderObject(cube_mesh, cubemap_material);

	// cube_object = new Engine::RenderObject(cube_mesh, cubemap_material);
	cube_object = new PickableObject(cube_mesh, cubemap_material);
	cube_object->SetPickingMat(picking_material);
	cube_object->SetIndex(2);

	Engine::Transform* cube_T = cube_object->GetTransform();
	cube_T->SetPosition(glm::vec3(1.5f, 0.8f, 0.0f));
	cube_T->SetScale(glm::vec3(0.5f));
	cube_object->SetTransform(*cube_T);

	// Light sun = Light();
	// sun.type = DirectionalLight;
	// sun.direction = glm::vec3(-0.2f, -1.0f, -0.3f);
	// sun.transform.SetPosition(glm::vec3(10.0f, 10.0f, 10.0f));
	// sun.diffuse_illuminance = glm::vec3(1.0f);
	// sun.ambient_illuminance = glm::vec3(0.1f);
	// sun.specular_illuminance = glm::vec3(0.5f);
	// lights.push_back(sun);

	// Light point_light = Light();
	// point_light.type = PointLight;
	// point_light.transform.SetPosition(glm::vec3(10.0f, 10.0f, 10.0f));
	// point_light.diffuse_illuminance = glm::vec3(1.0f,1.0f,1.0f);
	// point_light.ambient_illuminance = glm::vec3(0.1f, 0.1f, 0.1f);
	// point_light.specular_illuminance = glm::vec3(0.5f);
	// point_light.constant = 1.0f;
	// point_light.linear = 0.09f;
	// point_light.quadratic = 0.032f;
	// lights.push_back(point_light);

	Engine::Light spot_light = Engine::Light();
	spot_light.type = Engine::Spotlight;
	spot_light.transform.SetPosition(glm::vec3(0.0f,1.0f,5.0f));
	spot_light.diffuse_illuminance = glm::vec3(1.0f);
	spot_light.ambient_illuminance = glm::vec3(0.1f);
	spot_light.specular_illuminance = glm::vec3(0.5f);
	spot_light.constant = 1.0f;
	spot_light.linear = 0.09f;
	spot_light.quadratic = 0.032f;
	spot_light.direction = -glm::vec3(0.0f,1.0f,5.0f) + glm::vec3(-1.1f, 0.8f, 0.0f);
	spot_light.cutOff = glm::cos(glm::radians(10.0f));
	spot_light.outerCutOff = glm::cos(glm::radians(25.0f));
	lights.push_back(spot_light);
}


int main(int argc, char** argv)
{
	// Initialize GLFW library
	if (!glfwInit())
	{
		return -1;
	}

	// Create window and OpenGL context
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	/* Create a windowed mode window and its OpenGL context */
	g_window = glfwCreateWindow(g_window_width, g_window_height, "Some pin Renderer", NULL, NULL);
	if (!g_window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(g_window);

	// Initialize GLEW library
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		std::cout << "GLEW Error: " << glewGetErrorString(glew_error) << std::endl;
		exit(1);
	}


	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_CUBE_MAP);

	glfwSetInputMode(g_window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetMouseButtonCallback(g_window, MouseButtonCallback);
	glfwSetCursorPosCallback(g_window, CursorPosCallback);
	glfwSetKeyCallback(g_window, KeyboardCallback);
	glfwSetFramebufferSizeCallback(g_window, FramebufferResizeCallback);

	// Initialize framebuffer object and picking textures
	pickingInitialize(g_framebuffer_width, g_framebuffer_height);
	shadowInitialize(1024, 1024);


	InitScene();
	

	float prev_time = (float)glfwGetTime();

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(g_window) && glfwGetKey(g_window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		float total_time = (float)glfwGetTime();
		float elapsed_time = total_time - prev_time;
		prev_time = total_time;

		// First Pass: Object Selection
		// this is for picking the object using mouse interaction
		// binding framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, picking_fbo);
		// Background: RGB = 0x000000 => objectID: 0
		glClearColor((GLclampf) 0.0f, (GLclampf) 0.0f, (GLclampf) 0.0f, (GLclampf) 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* RenderPicking here */
		bunny_object->RenderPicking(main_camera);
		cube_object->RenderPicking(main_camera);

		/* Shadow Rendering */
		//glViewport(0, 0, 1024, 1024);
		//glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo);
		//glClearColor((GLclampf) 0.0f, (GLclampf) 0.0f, (GLclampf) 0.0f, (GLclampf) 0.0f);
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		////Do the rendering

		//bunny_object->Render(&lights[0]);



		// Second Pass: Object Rendering
		// Drawing object again
		glViewport(0,0,g_window_width,g_window_height);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor((GLclampf)(128. / 255.), (GLclampf)(200. / 255.), (GLclampf)(255. / 255.), (GLclampf)0.);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* Render Skybox here */
		
		glDisable(GL_CULL_FACE);
		glDepthMask(GL_FALSE);
		cubemap_material->UpdateLight(lights);
		cubemap_material->IsSkybox(1);
		cubemap_object->Render(main_camera);
		glEnable(GL_CULL_FACE);
		glDepthMask(GL_TRUE);


		material->UpdateLight(lights);
		

		material->UpdateDiffuseReflectance(glm::vec3(0.9f, 0.9f, 0.9f));
		bunny_object->Render(main_camera);

		material->UpdateDiffuseReflectance(glm::vec3(0.9f, 0.9f, 0.9f));
		cubemap_material->IsSkybox(0);
		cube_object->Render(main_camera);
			
		/* Swap front and back buffers */
		glfwSwapBuffers(g_window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	// Delete resources
	delete main_camera;

	glfwTerminate();
	return 0;

}