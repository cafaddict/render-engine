#version 330 core

in vec4 fragmentPosition;
in vec4 fragmentNormal;
in vec3 uv;

uniform samplerCube skybox;
out vec4 output_color;

uniform mat4 cameraTransform;

uniform bool is_skybox;

uniform sampler2D shadow;



//Future use
struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

struct Light
{
	int type;
	
	//Common
	vec3 ambient_illuminance;
	vec3 diffuse_illuminance;
	vec3 specular_illuminace;

	//Future use
	vec3 light_color;

	//Point Light = 1
	float constant;
	float linear;
	float quadratic;
	vec3 pos; //This also used for Spotlight

	//Directional Light = 2
	vec3 direction; //Tjos also used for Spotlight


	//Spotlight = 3
	float cutOff;
	float outerCutOff;
	
};

uniform Material material;

uniform int numLights;
uniform Light lights[100];

float ShadowCalculation(vec4 fragmentPosLight)
{
	//perform perspective divde
	vec3 proj_coords = fragmentPosLight.xyz / fragmentPosLight.w;
	proj_coords = proj_coords * 0.5 + 0.5;
	float closest_depth = texture(shadow, proj_coords.xy).r;
	float current_depth = proj_coords.z;
	float shadow = current_depth > closest_depth ? 1.0 : 0.0;

	return shadow;
}

void main()
{
	output_color = vec4(0);

	vec3 intensity = vec3(0);
	vec3 normal = normalize(fragmentNormal.xyz);

	vec3 viewDir = normalize(-fragmentPosition.xyz);

	mat4 worldToCamera = inverse(cameraTransform);

	vec3 tolight;

	int light_type;


	if(is_skybox)
	{
		

		for (int i=0; i<numLights; i++)
		{
			vec4 pos = worldToCamera * vec4(lights[i].pos, 1);
			
			float distance;
			float attenuation = 1.0f;

			light_type = lights[i].type;
			float theta = 1.0f;
			float cutOff = 0.0f;
			float epsilon = 0.0f;

			switch(light_type)
			{
				case 1:
					tolight = normalize(pos.xyz - fragmentPosition.xyz);
					distance = length(pos.xyz - fragmentPosition.xyz);
					attenuation = 1.0 / (lights[i].constant + lights[i].linear * distance + lights[i].quadratic * (distance * distance));
					break;
				case 2:
					tolight = (worldToCamera * vec4(lights[i].direction,1)).xyz;
					tolight = normalize (-tolight);
					break;
				case 3:
					tolight = normalize(pos.xyz - fragmentPosition.xyz);
					distance = length(pos.xyz - fragmentPosition.xyz);
					attenuation = 1.0 / (lights[i].constant + lights[i].linear * distance + lights[i].quadratic * (distance * distance));
					theta = dot(tolight, normalize(-lights[i].direction));
					cutOff = lights[i].cutOff;
					epsilon = cutOff - lights[i].outerCutOff;
					break;

			}

			
			float diffuse = max(0, dot(-normal, tolight));

			//specular
			vec3 halfwayDir = normalize(tolight + viewDir);
			float spec = pow(max(dot(-normal, halfwayDir), 0), 32);

			
			
			

			if(light_type == 3)
			{
				intensity += clamp((theta - lights[i].outerCutOff) / epsilon, 0.0, 1.0) * attenuation * (lights[i].diffuse_illuminance * diffuse + lights[i].specular_illuminace * spec);
				intensity += attenuation * lights[i].ambient_illuminance;
			}
			else
			{
				intensity += attenuation * (lights[i].diffuse_illuminance * diffuse + lights[i].ambient_illuminance + lights[i].specular_illuminace * spec);
			}

			
			
		}

		output_color = vec4(intensity, 1);
		output_color = output_color * texture(skybox, uv);
		output_color.rgb = pow(output_color.rgb, vec3(1.0/2.2));


	}
	else
	{
		vec3 I = normalize(fragmentPosition.xyz);
		vec3 R = reflect(I, normalize(fragmentNormal.xyz));
		output_color = texture(skybox, R);
	}
	
}
