#version 330 core

layout(location = 0) in vec4 pos;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec2 uv_coord;

uniform mat4 worldTransform;
uniform mat4 cameraTransform;
uniform mat4 projectionMatrix;
uniform vec4 color;
uniform bool is_skybox;

out vec4 fragmentPosition;
out vec4 fragmentNormal;
out vec3 uv;


uniform sampler2D shadow;

mat4 NormalMatrix(mat4 MVM)
{
	mat4 invm = inverse(MVM);
	invm[0][3] = 0;
	invm[1][3] = 0;
	invm[2][3] = 0;

	return transpose(invm);
}


void main()
{
	uv = vec3(pos);
	mat4 camT;
	if(is_skybox)
	{
		camT = mat4(mat3(cameraTransform));
		camT[3][3] = 1;

	}
	else
	{
		camT = cameraTransform;
	}
	// Output position of the vertex, in clip space : MVP * position
	mat4 MVM = inverse(camT) * worldTransform;
	mat4 NVM = NormalMatrix(MVM);
	vec4 wPosition = MVM * vec4(pos);
	fragmentPosition = wPosition;
	fragmentNormal = NVM * normal;
	gl_Position = projectionMatrix * wPosition;


}

