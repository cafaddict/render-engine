# RenderEngine

Windows에서 build 하는 법

Requirements
Visual Studio (https://visualstudio.microsoft.com/)
CMake (https://cmake.org/)
Git (https://git-scm.com/)
vcpkg (https://github.com/Microsoft/vcpkg)

How to install vcpkg
1. clone the vcpkg repository and go to vcpkg directory
2. Run batch file (bootstrap-vcpkg.bat) and build vcpkg.exe
3. Register the environment variables
3-1. PATH variable update (%USERPROFILE%\AppData\Local\Microsoft\WindowsApps)
3-2. VCPKG_DEFAULT_TRIPLET update (Variable name: VCPKG_DEFAULT_TRIPLET, Variable value: x64-windows)
4. User-wide integration. Requires admin on first use\
.\vcpkg.exe integrate install
5. install required packages
GLFW3: vcpkg install glfw3
GLEW: vcpkg install glew
GLM: vcpkg install glm
SOIL: vcpkg install soil
ASSIMP: vcpkg install assimp

Generate Build system from source using CMake

Linux에서 build 하는 법

컴파일러, 도구, 라이브러리 설치
cmake, make, g++, libx11-dev, libxi-dev libgl1-mesa-dev, libglu1-mesa-dev, libxrandr-dev, libxext-dev, libxi-dev, libglm-dev
libglew-dev, assimp-utils, libsoil-dev

#make 실행 이후에 Resource 파일을 build/Application으로 복사 이동시켜줘야함

mkdir build\
cd build\
cmake .. \
make all\
cd Application\
./App





